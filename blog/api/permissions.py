from rest_framework.permissions import BasePermission
from core.models import ApiKey


class IsApiKeyProvided(BasePermission):
    def has_permission(self, request, view):
        return bool(request.path.startswith('/api') and ApiKey.objects.filter(key=request.headers.get('X-Api-Key', None)).exists())