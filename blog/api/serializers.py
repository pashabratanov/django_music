from rest_framework import serializers
from django.contrib.auth import get_user_model
from core.models import Category, Post, Tag
from django.utils import timezone


class CategorySerializer(serializers.Serializer):
    name = serializers.CharField()

    def save(self):
        return Category.objects.create(**self.validated_data)


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ['id', 'name']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['id', 'username']


class PostSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True)
    # author = UserSerializer()
    tags_count = serializers.SerializerMethodField()
    days_ago = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = '__all__'

    def get_tags_count(self, obj):
        return obj.tags.all().count()

    def get_days_ago(self, obj):
        if obj.publish_date:
            return (timezone.now() - obj.publish_date).days
        return 0

    def update(self, instance, validated_data, *args, **kwargs):
        tags_name = [x['name'] for x in validated_data.pop('tags')]
        instance.tags.clear()
        tags = Tag.objects.filter(name__in=tags_name)
        instance.tags.add(*tags)

        for field, value in validated_data.items():
            setattr(instance, field, value)

        instance.save()

        return instance
#
# class PostCreateSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Post
#         fields = '__all__'
