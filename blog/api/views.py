from api.serializers import CategorySerializer, PostSerializer
    # , PostCreateSerializer
from rest_framework import views, viewsets, mixins
from rest_framework.response import Response
from rest_framework import authentication, permissions
from core.models import Category, Post


class CategoryApiView(views.APIView):
    authentication_classes = [authentication.TokenAuthentication]

    def get(self, request):
        categories = Category.objects.all()
        serializer = CategorySerializer(categories, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = CategorySerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors)


class PostApiView(viewsets.ModelViewSet):
    serializer_class = PostSerializer
    # detail_serializer = PostSerializer
    # create_serializer = PostCreateSerializer

    queryset = Post.objects.all()

    # def dispatch(self, request, *args, **kwargs):
    #     if request.method.lower() in ['post', 'put', 'patch']:
    #         self.serializer_class = self.create_serializer
    #     return super().dispatch(request, *args, **kwargs)

    # def update(self, request, *args, **kwargs):
    #     super().update(request, *args, **kwargs)
    #     return Response(self.detail_serializer(self.get_object()).data)
