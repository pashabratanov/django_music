"""blog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from core import views
from django.contrib.auth.views import LoginView, LogoutView
from api import views as api_views
from rest_framework.authtoken import views as drf_views
from rest_framework import routers
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns

router = routers.DefaultRouter()
router.register('posts', api_views.PostApiView)


urlpatterns = i18n_patterns(
    path('admin/', admin.site.urls),
    path('', views.IndexView.as_view()),
    path('create/post/', views.PostCreateView.as_view()),
    path('update/post/<int:id>/', views.PostUpdateView.as_view()),
    path('login/', views.LoginView.as_view()),
    # path('login/', LoginView.as_view(template_name='login.html', next_page='/', model=get_user_model())),
    path('logout/', LogoutView.as_view(next_page='/')),
    path('refresh/', views.PageRefreshCountView.as_view()),
    path('registration/', views.RegistrationView.as_view()),
    path('profile/<int:id>', views.ProfileView.as_view()),
    path('api-auth/', include('rest_framework.urls')),
    path('api/categories/', api_views.CategoryApiView.as_view()),
    path('api-token-auth/', drf_views.obtain_auth_token),
    # path('api/posts/', api_views.PostApiView.as_view()),
    path('api/', include(router.urls)),
    path('tag-rating/', views.TagRating.as_view()),
    path('js/', views.JsView.as_view()),
    path('chat/rooms/', views.RoomListView.as_view()),
    path('chat/<int:room_id>/', views.ChatView.as_view()),
    path('send/money/', views.SendMoney.as_view(), name='send_money'),
)

urlpatterns += [
    path('lang/', views.LangChangeView.as_view()),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    urlpatterns.append(path('__debug__/', include('debug_toolbar.urls')))
