import json
from channels.generic.websocket import WebsocketConsumer
from django.contrib.auth import get_user_model
from asgiref.sync import async_to_sync

from core.models import Messages


class ChatConsumer(WebsocketConsumer):
    def connect(self):
        self.room_id = self.scope["url_route"]["kwargs"]["room_id"]
        self.group_name = f'room_{self.room_id}'

        async_to_sync(self.channel_layer.group_add)(
            self.group_name,
            self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        print('DISCONNECTED')
        async_to_sync(self.channel_layer.group_discard)(
            self.group_name,
            self.channel_name
        )

    def receive(self, text_data):
        data = json.loads(text_data)
        username = get_user_model().objects.get(id=data['user_id']).username

        if data['type'] == 'connected':

            async_to_sync(self.channel_layer.group_send)(
                self.group_name,
                {
                    'type': 'chat_connected',
                    'message': f'{username} is joined to chat',
                }
            )
        else:
            Messages.objects.create(room_id=self.room_id,
                                    user_from_id=data['user_id'],
                                    message=data['message'])

            async_to_sync(self.channel_layer.group_send)(
                self.group_name,
                {
                    'type': 'chat_message',
                    'message': json.dumps({'text': data['message'], 'username': username})
                }
            )

    def chat_connected(self, event):
        message = event['message']

        self.send(text_data=json.dumps({
            'message': message,
            'type': 'connected',
        }))

    def chat_message(self, event):
        message = event['message']

        self.send(text_data=json.dumps({
            'message': message,
            'type': 'message',
        }))
