from django.urls import path

from chat import consumers

ws_routes = {
    path('ws/chat/<int:room_id>/', consumers.ChatConsumer.as_asgi()),
}
