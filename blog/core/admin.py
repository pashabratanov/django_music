from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
# from core.models import ALL_MODELS
from core.models import BlogUser, Post, Tag, Category, ApiKey, TagPost, Rooms
from django.utils.translation import gettext_lazy as _
from django.template.defaultfilters import safe
from core.forms import PostForm, PostAdminForm
from django.utils import timezone
from django_extensions.admin import ForeignKeyAutocompleteAdmin
from modeltranslation.admin import TranslationAdmin

# class ProfileAdmin(admin.StackedInline):
#     model = Profile
#
#
# class UserProfileAdmin(UserAdmin):
#     inlines = [ProfileAdmin]

# for model in ALL_MODELS:
#     admin.site.register(model)

# admin.site.unregister(User)
# admin.site.register(User, UserProfileAdmin)


class UserProfileAdmin(UserAdmin):
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (_("Personal info"), {"fields": ("first_name", "last_name", "email", "phone")}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
        (_("Other"), {"fields": ("subscribers", "money",)}),
    )
    list_display = ('username', 'first_name', 'last_name', 'email', 'money', 'is_superuser',)


class PostInline(admin.TabularInline):
    model = Post


class CategoryAdmin(TranslationAdmin):
    model = Category
    inlines = [PostInline]

    class Media:
        js = (
            "https://code.jquery.com/ui/1.13.2/jquery-ui.min.js",
            'modeltranslation/js/force_jquery.js',
            'modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
        }


class TagInline(admin.TabularInline):
    model = Post.tags.through


class PostAdmin(ForeignKeyAutocompleteAdmin):
    list_display = ('title', 'get_short_body', 'user_link', 'is_published', 'publish_date', 'created_at')
    list_filter = ('is_published', 'publish_date', 'tags')
    list_select_related = ('author',)
    inlines = [TagInline]

    related_search_fields = {'author': ('username', 'id', 'email')}

    # readonly_fields = ('publish_date', 'author')

    actions = ['publish']
    form = PostAdminForm

    fieldsets = (
        ('Base data', {'fields': ('title', 'body', 'rating', 'cover')}),
        ('Author', {'fields': ('author',)}),
        ('Publish Data', {'fields': ('is_published', 'publish_date')}),
        # ('Tags', {'fields': ('tags',)})
    )

    def user_link(self, obj):
        return safe(f'<a href="/admin/core/bloguser/{obj.author.id}/change/" target="_blank">{obj.author.username}</a>')

    user_link.short_description = 'author'
    user_link.admin_order_field = 'author__id'

    def publish(self, request, queryset):
        queryset.filter(is_published=False).update(is_published=True, publish_date=timezone.now())


class TagAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)
    list_editable = ('name',)


class ApiKeyAdmin(admin.ModelAdmin):
    list_display = ('client_name', 'key',)
    readonly_fields = ('key', 'created_at', 'updated_at',)


admin.site.register(BlogUser, UserProfileAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(ApiKey, ApiKeyAdmin)
admin.site.register(TagPost)
admin.site.register(Rooms)
