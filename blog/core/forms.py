from django import forms
from django.utils import timezone
from core.models import Category, Tag, Post
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth import get_user_model
from core.widgets import ImageWidget
from django.utils.translation import gettext, gettext_lazy as _
from django.db import transaction

# class PostForm(forms.Form):
#     YES = 1
#     NO = 0
#
#     PUBLISH_CHOICES = (
#         (YES, 'Yes'),ls
#         (NO, 'No'),
#     )
#
#     title = forms.CharField()
#     body = forms.CharField(widget=forms.widgets.Textarea(attrs={"cols": "50", "rows": "10"}))
#     category = forms.ModelChoiceField(
#         queryset=Category.objects.all(),
#     )
#
#     tags = forms.ModelMultipleChoiceField(
#         queryset=Tag.objects.all(),
#         widget=forms.widgets.CheckboxSelectMultiple
#     )
#
#     publish = forms.ChoiceField(choices=PUBLISH_CHOICES, widget=forms.widgets.RadioSelect(), initial=NO)
#
#     def save(self, user):
#         post = Post(
#             title=self.cleaned_data['title'],
#             body=self.cleaned_data['body'],
#             category=self.cleaned_data['category'],
#             author=user
#         )
#         if int(self.cleaned_data['publish']) == self.YES:
#             print(123)
#             post.is_published = True
#             post.publish_date = timezone.now()
#
#         post.save()
#
#         post.tags.add(*self.cleaned_data['tags'])


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        exclude = ('publish_date', 'author')
        widgets = {
            'tags': forms.widgets.CheckboxSelectMultiple,
            'cover': ImageWidget
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(PostForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(PostForm, self).clean()
        title = cleaned_data.get('title')
        body = cleaned_data.get('body')
        if body and title:
            if body == title:
                raise forms.ValidationError('Title and body cant be the same')

    # def clean_title(self):
    #     data = self.cleaned_data['title']
    #
    #     if 'a' in data.lower():
    #         raise forms.ValidationError('A is no longer allowed.')
    #     return data

    def save(self, *args, **kwargs):
        post = super(PostForm, self).save(commit=False)

        if self.cleaned_data['is_published']:
            post.publish_date = timezone.now()
        print(post.cover)
        post.author = self.user
        post.save()
        self.save_m2m()
        return post


class LoginForm(forms.Form):
    username = forms.CharField(label=_('Username'))
    password = forms.CharField(widget=forms.widgets.PasswordInput(), label=_('Password'))

    def clean(self):
        # try:
        #     User.objects.values('id').get(
        #         username=self.cleaned_data['username'],
        #         password=self.cleaned_data['password']
        #     )
        # except User.DoesNotExist:
        #     raise forms.ValidationError('Incorrect username/password')

        user = get_user_model().objects.filter(
            username=self.cleaned_data['username'],
        ).first()

        if not user or not user.check_password(self.cleaned_data['password']):
            raise forms.ValidationError(_('Incorrect username/password'))

    def auth(self, request):
        user = authenticate(request, **self.cleaned_data)

        login(request, user)
        return user


class RegistrationForm(forms.ModelForm):
    password = forms.CharField(widget=forms.widgets.PasswordInput)
    check_password = forms.CharField(widget=forms.widgets.PasswordInput)

    class Meta:
        model = get_user_model()
        fields = ['first_name', 'last_name', 'username', 'email']

    def clean_check_password(self):
        if self.cleaned_data['password'] != self.cleaned_data['check_password']:
            raise forms.ValidationError('Passwords do not match')
        return self.cleaned_data['check_password']

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['password'])
        user.save()
        return user


class PostAdminForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = '__all__'
        widgets = {
            'tags': forms.widgets.CheckboxSelectMultiple,
            'cover': ImageWidget
        }


class SendMoneyForm(forms.Form):
    amount = forms.IntegerField()
    user_to = forms.ModelChoiceField(queryset=get_user_model().objects.all())

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super().__init__(*args, **kwargs)

    def clean(self):
        if self.user.money < self.cleaned_data['amount']:
            print('close to error')
            raise forms.ValidationError('You do not have such amount of money!')
        return self.cleaned_data

    def save(self):
        with transaction.atomic():
            self.user.money -= self.cleaned_data['amount']
            self.user.save()

            self.cleaned_data['user_to'].money += self.cleaned_data['amount']
            self.cleaned_data['user_to'].save()

