from django.core.management.base import BaseCommand
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('permission_name', type=str)

    def handle(self, *args, **options):
        permission_name = options['permission_name']
        print(permission_name)

        c_type = ContentType.objects.get_or_create(
            app_label='core',
            model='custom_permission'
        )[0]

        Permission.objects.create(
            name=permission_name.replace('_', ' ').title(),
            content_type=c_type,
            codename=permission_name
        )

