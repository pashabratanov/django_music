from django.utils.deprecation import MiddlewareMixin
from core.models import ApiKey
from django.http import JsonResponse


class ApiKeyCheckMiddleware(MiddlewareMixin):
    def process_request(self, request):
        if request.path.startswith('/api') and not ApiKey.objects.filter(key=request.headers.get('X-Api-Key', None)).exists():
            return JsonResponse({'error': 'Api Key was not provided or wrong'}, status=401)


