# Generated by Django 4.0.4 on 2022-06-05 18:07

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_bloguser_subscribers_alter_post_author'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bloguser',
            name='subscribers',
            field=models.ManyToManyField(blank=True, to=settings.AUTH_USER_MODEL),
        ),
    ]
