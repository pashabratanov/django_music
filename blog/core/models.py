import binascii
import os

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth import get_user_model
from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete
from django.db.models.aggregates import Avg, Count, Sum
# from core.tasks import send_notification_to_subscribers, clear_nav_cache
# to delete template cache
from django.core.cache.utils import make_template_fragment_key
from django.core.cache import cache
from django.utils.translation import gettext, gettext_lazy as _


class BlogUser(AbstractUser):
    phone = models.CharField(max_length=13, blank=True, null=True, verbose_name=_('phone'))
    subscribers = models.ManyToManyField('core.BlogUser', blank=True, verbose_name=_('subscribers'))
    money = models.PositiveIntegerField(default=0)

    REQUIRED_FIELDS = ['email', 'phone']


class TimeIt(models.Model):
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, verbose_name=_('updated at'))

    class Meta:
        abstract = True


class NameIt(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class NamedManger(models.Manager):
    def get_statistic(self):
        return self.get_queryset().annotate(
            post_rating=Avg('post__rating'),
            all_rating=Sum('post__rating'),
            post_count=Count('post__rating')
        ).filter(post_count__gt=0)


class Tag(TimeIt, NameIt):
    objects = NamedManger()


class Category(TimeIt, NameIt):
    objects = NamedManger()

    class Meta:
        verbose_name_plural = _('Categories')

    def save(self, *args, **kwargs):
        # clear_nav_cache.delay()
        instance = super().save(*args, **kwargs)
        return instance



class TagPost(models.Model):
    post = models.ForeignKey('core.Post', on_delete=models.CASCADE)
    tag = models.ForeignKey('core.Tag', on_delete=models.CASCADE)
    is_promoted = models.BooleanField(default=False)

    class Meta:
        unique_together = ('post', 'tag')


class PostManager(models.Manager):
    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(rating__gt=0)

    def get_by_id(self, id):
        return self.get_queryset().filter(id=id).first()

    def get_avg_rating(self):
        return self.get_queryset().all().aggregate(
            post_avg_rating=Avg('rating')
        )['post_avg_rating']


def post_upload_to(obj, file_name):
    return f'posts/{obj.id}/{file_name}'


class Post(TimeIt):
    title = models.CharField(max_length=255, unique=True, db_index=True)
    body = models.TextField()
    publish_date = models.DateTimeField(blank=True, null=True)
    is_published = models.BooleanField(default=False)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True, db_index=True)
    # author = models.ForeignKey(User, on_delete=models.CASCADE) TODO NEVER MAKE FK TO USER MODEL DIRECTLY
    author = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='posts')
    tags = models.ManyToManyField("core.Tag", blank=True, through=TagPost)
    rating = models.IntegerField(default=0)
    cover = models.ImageField(blank=True, null=True, upload_to=post_upload_to)

    objects = PostManager()
    original = models.Manager()

    class Meta:
        permissions = (
            ('can_add_comment', 'Is user can add comment'),
            ('can_update_post', 'Is user can update post'),
        )
        unique_together = ('title', 'body')
        index_together = [
            ['title', 'category']
        ]

    def __str__(self):
        return self.title

    def get_short_body(self):
        return self.body[:20]

    get_short_body.short_description = 'short body'

    def save(self, *args, **kwargs):
        if self.pk:  # update
            cache_key = make_template_fragment_key('post_cache', [self.pk])
            cache.delete(cache_key)
        else:  # create
            pass

        instance = super().save(*args, **kwargs)
        return instance


class Comment(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    text = models.TextField()


# class Profile(models.Model):
#     phone = models.CharField(max_length=14)
#
#     user = models.OneToOneField(User, on_delete=models.CASCADE)


# @receiver(post_delete, sender=Category)
# def reset_category_cache(**kwargs):
#     clear_nav_cache.delay()


# @receiver(post_save, sender=Post)
# def post_create_handler(instance, **kwargs):
#     if instance.is_published:
#         send_notification_to_subscribers.delay(instance.id)


class ApiKey(TimeIt):
    client_name = models.CharField(max_length=255)
    key = models.CharField(max_length=255)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super().save(*args, **kwargs)

    @classmethod
    def generate_key(cls):
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return self.client_name


class Rooms(TimeIt):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Messages(TimeIt):
    room = models.ForeignKey(Rooms, on_delete=models.CASCADE, null=True)
    user_from = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    message = models.TextField(max_length=255)

    def __str__(self):
        return self.user_from.username


ALL_MODELS = [Category, Post, Tag]
