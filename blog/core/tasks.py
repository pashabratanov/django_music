from django.core.mail import send_mail
# from blog.celery import app
from django.contrib.auth import get_user_model
from django.core.cache.utils import make_template_fragment_key
from django.core.cache import cache

#
# @app.task
# def send_notification_to_subscribers(post_id):
#     from core.models import Post
#     post = Post.objects.get(id=post_id)
#
#     for subscriber in post.author.subscribers.all():
#         send_mail(
#             f'New post of {post.author.username}',
#             f'new post of {post.author.username} has been published!',
#             'admin@example.com',
#             recipient_list=[subscriber.email]
#         )
#
#
# @app.task
# def clear_nav_cache():
#     for username in get_user_model().objects.all().values_list('username', flat=True):
#         key = make_template_fragment_key('nav', [username])
#         cache.delete(key)
#
#
# @app.task
# def say_hello():
#     print('hello')

