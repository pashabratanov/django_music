from django import template

from core.models import Post

register = template.Library()


@register.filter
def color_it(text, color):
    return f'<span style="color: {color}">{text}</span>'


@register.simple_tag
def double_sum(first, second):
    return first + second


@register.inclusion_tag('includes/post.html')
def random_post(user):
    post = Post.objects.all().order_by('?').first()
    if not post:
        return {}
    return {'post': post, 'user': user}


@register.filter
def cut_lang(url):
    return url[3:]
