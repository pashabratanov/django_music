from django.test import TestCase, Client
from django.contrib.auth import get_user_model

from core.models import Post
from core.views import TagRating


class ProfileViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()

        self.user = get_user_model().objects.create(username='test')
        self.user.set_password('test')
        self.user.save()

        self.client.login(username='test', password='test')

    # def tearDown(self):
    #     pass

    def test_diagnostic(self):
        response = self.client.post(f'/profile/{self.user.id}')
        self.assertTrue(response.status_code != 404)

    def test_diagnostic_not_found(self):
        response = self.client.post('/profile/777/')
        self.assertEqual(response.status_code, 404)

    def test_diagnostic_get(self):
        response = self.client.post(f'/profile/{self.user.id}')
        self.assertTrue(response.status_code == 200)

    def test_subscribe(self):
        user = get_user_model().objects.create(username='test1')

        response = self.client.post(f'/profile/{user.id}')
        self.assertEqual(user.subscribers.all().count(), 1)
        self.assertEqual(user.subscribers.all().first().id, self.user.id)
        self.assertTrue((response.json()['is_subscribe']))

    def test_unsubscribe(self):
        user = get_user_model().objects.create(username='test1')
        user.subscribers.add(self.user)
        user.save()

        response = self.client.post(f'/profile/{user.id}')
        self.assertEqual(user.subscribers.all().count(), 0)
        self.assertFalse(response.json()['is_subscribe'])


class TagRatingTestCase(TestCase):
    def test_get_context_data(self):
        expected_value = 5.0
        user = get_user_model().objects.create(username='test1')

        for counter in range(0, 10):
            Post.objects.create(
                title=f't{counter}',
                body=f'test{counter}',
                rating=counter,
                author=user
            )
        view = TagRating()
        view.object_list = []
        value = view.get_context_data()['post_avg_rating']
        self.assertEqual(expected_value, value)
