from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import TemplateView, View
from django.views.generic.edit import FormView, CreateView, UpdateView
from core.models import Post, Tag, Category
from core.forms import PostForm, LoginForm, RegistrationForm, SendMoneyForm
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import HttpResponseForbidden
from django.views.generic.detail import DetailView
from django.contrib.auth import get_user_model
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.db.models.aggregates import Avg, Sum, Count
from django.views.generic.list import ListView
from django.views.decorators.cache import never_cache
from django.utils.translation import gettext, gettext_lazy as _
from django.views.generic.base import RedirectView
from core.models import Messages, Rooms
from django.utils import translation
from django.conf import settings
from django.urls import reverse_lazy


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)

        posts = Post.original.all().distinct().order_by('-title').select_related('category').prefetch_related('tags')

        context.update({'posts': posts})
        context['refresh_count'] = self.request.session.get('refresh_count', 0)
        context['test_text'] = _('Test text to translate')

        return context

    def post(self, request):
        print(self.request.method)
        return self.render_to_response({'first': 'f', 'second': 's'})


# @never_cache
class PostCreateView(LoginRequiredMixin, CreateView):
    template_name = 'post_create.html'
    model = Post
    form_class = PostForm
    success_url = '/'
    login_url = '/login/'

    def get_form_kwargs(self):
        form_kwargs = super(PostCreateView, self).get_form_kwargs()
        form_kwargs['user'] = self.request.user
        return form_kwargs


class PostUpdateView(PermissionRequiredMixin, LoginRequiredMixin, UpdateView):
    template_name = 'post_create.html'
    model = Post
    form_class = PostForm
    success_url = '/'
    pk_url_kwarg = 'id'
    permission_required = 'core.can_update_post'

    # allow edit post only its author
    def dispatch(self, request, *args, **kwargs):
        post = self.get_object()
        if post.author_id != request.user.id:
            return HttpResponseForbidden("<h1>This is not your post </h1>")
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        form_kwargs = super(PostUpdateView, self).get_form_kwargs()
        form_kwargs['user'] = self.request.user
        return form_kwargs


class LoginView(FormView):
    form_class = LoginForm
    template_name = 'login.html'
    success_url = '/'

    def form_valid(self, form):
        form.auth(self.request)
        return super().form_valid(form)


class PageRefreshCountView(TemplateView):
    template_name = 'refresh_count.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        count = self.request.session.get('refresh_count', 0)

        count += 1

        self.request.session['refresh_count'] = count

        context['refresh_count'] = count
        return context


class RegistrationView(FormView):
    template_name = 'registration.html'
    form_class = RegistrationForm
    success_url = '/login/'

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


class ProfileView(DetailView):
    model = get_user_model()
    template_name = 'profile.html'
    pk_url_kwarg = 'id'

    def post(self, request, id):
        current_user = get_object_or_404(get_user_model(), id=id)
        is_subscribe = False

        if request.user in current_user.subscribers.all():
            current_user.subscribers.remove(request.user)
        else:
            current_user.subscribers.add(request.user)
            is_subscribe = True
        current_user.save()
        return JsonResponse({'is_subscribe': is_subscribe})

# @method_decorator(csrf_exempt, 'dispatch')
# class CategoryApiView(View):
#     def get(self, request):
#         categories = [{'name': x.name} for x in Category.objects.all()]
#         return JsonResponse(categories, safe=False)
#
#     def post(self, request):
#         return JsonResponse({}, safe=False)


class TagRating(ListView):
    template_name = 'tag_rating.html'

    model = Tag

    context_object_name = 'tags'

    def get_queryset(self):
        return self.model.objects.get_statistic()

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        context['post_avg_rating'] = Post.objects.get_avg_rating()
        return context


class JsView(TemplateView):
    template_name = 'js.html'


class ChatView(TemplateView):
    template_name = 'chat.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['room'] = get_object_or_404(Rooms, id=self.kwargs['room_id'])
        context['messages'] = Messages.objects.filter(room_id=self.kwargs['room_id'])[:50]
        return context


class RoomListView(ListView):
    template_name = 'rooms.html'
    model = Rooms
    context_object_name = 'rooms'


class LangChangeView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        return self.request.GET.get('next', '/')

    def get(self, request, *args, **kwargs):
        user_lang = request.GET.get('lang', 'en')
        translation.activate(user_lang)

        response = super().get(request, *args, **kwargs)
        response.set_cookie(settings.LANGUAGE_COOKIE_NAME, user_lang)
        return response


class SendMoney(LoginRequiredMixin, FormView):
    template_name = 'send_money.html'
    form_class = SendMoneyForm
    success_url = reverse_lazy('send_money')

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        form_kwargs['user'] = self.request.user
        return form_kwargs

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)
