from django.forms import FileInput


class ImageWidget(FileInput):
    template_name = 'widgets/input.html'

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context['value'] = value
        return context

