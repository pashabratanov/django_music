#!/usr/bin/env python


# This file has been automatically generated.
# Instead of changing it, create a file called import_helper.py
# and put there a class called ImportHelper(object) in it.
#
# This class will be specially casted so that instead of extending object,
# it will actually extend the class BasicImportHelper()
#
# That means you just have to overload the methods you want to
# change, leaving the other ones intact.
#
# Something that you might want to do is use transactions, for example.
#
# Also, don't forget to add the necessary Django imports.
#
# This file was generated with the following command:
# manage.py dumpscript core
#
# to restore it, run
# manage.py runscript module_name.this_script_name
#
# example: if manage.py is at ./manage.py
# and the script is at ./some_folder/some_script.py
# you must make sure ./some_folder/__init__.py exists
# and run  ./manage.py runscript some_folder.some_script
import os, sys
from django.db import transaction

class BasicImportHelper:

    def pre_import(self):
        pass

    @transaction.atomic
    def run_import(self, import_data):
        import_data()

    def post_import(self):
        pass

    def locate_similar(self, current_object, search_data):
        # You will probably want to call this method from save_or_locate()
        # Example:
        #   new_obj = self.locate_similar(the_obj, {"national_id": the_obj.national_id } )

        the_obj = current_object.__class__.objects.get(**search_data)
        return the_obj

    def locate_object(self, original_class, original_pk_name, the_class, pk_name, pk_value, obj_content):
        # You may change this function to do specific lookup for specific objects
        #
        # original_class class of the django orm's object that needs to be located
        # original_pk_name the primary key of original_class
        # the_class      parent class of original_class which contains obj_content
        # pk_name        the primary key of original_class
        # pk_value       value of the primary_key
        # obj_content    content of the object which was not exported.
        #
        # You should use obj_content to locate the object on the target db
        #
        # An example where original_class and the_class are different is
        # when original_class is Farmer and the_class is Person. The table
        # may refer to a Farmer but you will actually need to locate Person
        # in order to instantiate that Farmer
        #
        # Example:
        #   if the_class == SurveyResultFormat or the_class == SurveyType or the_class == SurveyState:
        #       pk_name="name"
        #       pk_value=obj_content[pk_name]
        #   if the_class == StaffGroup:
        #       pk_value=8

        search_data = { pk_name: pk_value }
        the_obj = the_class.objects.get(**search_data)
        #print(the_obj)
        return the_obj


    def save_or_locate(self, the_obj):
        # Change this if you want to locate the object in the database
        try:
            the_obj.save()
        except:
            print("---------------")
            print("Error saving the following object:")
            print(the_obj.__class__)
            print(" ")
            print(the_obj.__dict__)
            print(" ")
            print(the_obj)
            print(" ")
            print("---------------")

            raise
        return the_obj


importer = None
try:
    import import_helper
    # We need this so ImportHelper can extend BasicImportHelper, although import_helper.py
    # has no knowlodge of this class
    importer = type("DynamicImportHelper", (import_helper.ImportHelper, BasicImportHelper ) , {} )()
except ImportError as e:
    # From Python 3.3 we can check e.name - string match is for backward compatibility.
    if 'import_helper' in str(e):
        importer = BasicImportHelper()
    else:
        raise

import datetime
from decimal import Decimal
from django.contrib.contenttypes.models import ContentType

try:
    import dateutil.parser
    from dateutil.tz import tzoffset
except ImportError:
    print("Please install python-dateutil")
    sys.exit(os.EX_USAGE)

def run():
    importer.pre_import()
    importer.run_import(import_data)
    importer.post_import()

def import_data():
    # Initial Imports
    from django.contrib.auth.models import Permission

    # Processing model: core.models.Tag

    from core.models import Tag


    # Processing model: core.models.Category

    from core.models import Category

    core_category_1 = Category()
    core_category_1.created_at = dateutil.parser.parse("2022-08-16T12:19:01.380477+00:00")
    core_category_1.updated_at = dateutil.parser.parse("2022-08-16T12:19:01.380522+00:00")
    core_category_1.name = 'music'
    core_category_1 = importer.save_or_locate(core_category_1)

    core_category_2 = Category()
    core_category_2.created_at = dateutil.parser.parse("2022-08-16T12:19:07.300880+00:00")
    core_category_2.updated_at = dateutil.parser.parse("2022-08-16T12:19:07.300917+00:00")
    core_category_2.name = 'sport'
    core_category_2 = importer.save_or_locate(core_category_2)

    core_category_3 = Category()
    core_category_3.created_at = dateutil.parser.parse("2022-08-16T12:19:25.713623+00:00")
    core_category_3.updated_at = dateutil.parser.parse("2022-08-16T12:19:25.713677+00:00")
    core_category_3.name = 'python'
    core_category_3 = importer.save_or_locate(core_category_3)

    core_category_4 = Category()
    core_category_4.created_at = dateutil.parser.parse("2022-08-16T12:19:35.448118+00:00")
    core_category_4.updated_at = dateutil.parser.parse("2022-08-16T12:19:35.448164+00:00")
    core_category_4.name = 'JavaScript'
    core_category_4 = importer.save_or_locate(core_category_4)

    core_category_5 = Category()
    core_category_5.created_at = dateutil.parser.parse("2022-08-16T12:20:09.553909+00:00")
    core_category_5.updated_at = dateutil.parser.parse("2022-08-16T12:20:09.553971+00:00")
    core_category_5.name = 'travel'
    core_category_5 = importer.save_or_locate(core_category_5)

    core_category_6 = Category()
    core_category_6.created_at = dateutil.parser.parse("2022-08-16T12:20:26.816736+00:00")
    core_category_6.updated_at = dateutil.parser.parse("2022-08-16T12:20:26.816781+00:00")
    core_category_6.name = 'lord of rings'
    core_category_6 = importer.save_or_locate(core_category_6)

    # Processing model: core.models.ApiKey

    from core.models import ApiKey


    # Processing model: core.models.Rooms

    from core.models import Rooms

    core_rooms_1 = Rooms()
    core_rooms_1.created_at = dateutil.parser.parse("2022-08-17T18:21:47.193352+00:00")
    core_rooms_1.updated_at = dateutil.parser.parse("2022-08-17T18:21:47.193399+00:00")
    core_rooms_1.name = 'Test'
    core_rooms_1 = importer.save_or_locate(core_rooms_1)

    core_rooms_2 = Rooms()
    core_rooms_2.created_at = dateutil.parser.parse("2022-08-17T18:22:07.568458+00:00")
    core_rooms_2.updated_at = dateutil.parser.parse("2022-08-17T18:22:07.568517+00:00")
    core_rooms_2.name = 'New Room'
    core_rooms_2 = importer.save_or_locate(core_rooms_2)

    # Processing model: core.models.Comment

    from core.models import Comment

    # Processing model: core.models.BlogUser

    from core.models import BlogUser

    core_bloguser_1 = BlogUser()
    core_bloguser_1.password = 'pbkdf2_sha256$320000$x2xzgcTMmTUgWFhOG613PO$38+8ukUdv+tbSG6bXTX9BvlKpbVuS6HiszjgzRZ9tYs='
    core_bloguser_1.last_login = dateutil.parser.parse("2022-08-18T16:33:57.221997+00:00")
    core_bloguser_1.is_superuser = True
    core_bloguser_1.username = 'pasha'
    core_bloguser_1.first_name = ''
    core_bloguser_1.last_name = ''
    core_bloguser_1.email = 'pasha@example.com'
    core_bloguser_1.is_staff = True
    core_bloguser_1.is_active = True
    core_bloguser_1.date_joined = dateutil.parser.parse("2022-08-03T05:55:11.639419+00:00")
    core_bloguser_1.phone = None
    core_bloguser_1 = importer.save_or_locate(core_bloguser_1)

    core_bloguser_2 = BlogUser()
    core_bloguser_2.password = 'pbkdf2_sha256$320000$jYBXwaHP49RUk6UdXkma66$t/bbfidGZ7xVmNlAGM/+s5BkmR3Dj953vtYtAMqZpOU='
    core_bloguser_2.last_login = dateutil.parser.parse("2022-08-18T14:53:28.422296+00:00")
    core_bloguser_2.is_superuser = False
    core_bloguser_2.username = 'dima'
    core_bloguser_2.first_name = 'Дмитро'
    core_bloguser_2.last_name = 'Шпак'
    core_bloguser_2.email = 'dima@example.com'
    core_bloguser_2.is_staff = False
    core_bloguser_2.is_active = True
    core_bloguser_2.date_joined = dateutil.parser.parse("2022-08-03T18:56:24+00:00")
    core_bloguser_2.phone = None
    core_bloguser_2 = importer.save_or_locate(core_bloguser_2)

    core_bloguser_3 = BlogUser()
    core_bloguser_3.password = 'pbkdf2_sha256$320000$SU17eu9lHo34hUIxyl5Yj8$TNxQzxHoKCYZSlBrkvzF71Db15IAlApUuwPmp4AV64c='
    core_bloguser_3.last_login = None
    core_bloguser_3.is_superuser = True
    core_bloguser_3.username = 'gosha'
    core_bloguser_3.first_name = ''
    core_bloguser_3.last_name = ''
    core_bloguser_3.email = 'gosha@mail.com'
    core_bloguser_3.is_staff = True
    core_bloguser_3.is_active = True
    core_bloguser_3.date_joined = dateutil.parser.parse("2022-08-19T14:07:46.370255+00:00")
    core_bloguser_3.phone = '380633295634'
    core_bloguser_3 = importer.save_or_locate(core_bloguser_3)

    # Processing model: core.models.TagPost

    from core.models import TagPost


    # Processing model: core.models.Post

    from core.models import Post

    core_post_1 = Post()
    core_post_1.created_at = dateutil.parser.parse("2022-08-16T12:21:51.141229+00:00")
    core_post_1.updated_at = dateutil.parser.parse("2022-08-16T12:21:51.141301+00:00")
    core_post_1.title = 'my first post'
    core_post_1.body = 'aaaaa'
    core_post_1.publish_date = dateutil.parser.parse("2022-08-16T12:21:51.140557+00:00")
    core_post_1.is_published = True
    core_post_1.category = core_category_4
    core_post_1.author = core_bloguser_1
    core_post_1.rating = 5
    core_post_1.cover = 'posts/None/Screenshot_from_2022-05-25_20-06-43.png'
    core_post_1 = importer.save_or_locate(core_post_1)

    core_post_2 = Post()
    core_post_2.created_at = dateutil.parser.parse("2022-08-16T12:22:34.060069+00:00")
    core_post_2.updated_at = dateutil.parser.parse("2022-08-16T12:22:34.060131+00:00")
    core_post_2.title = 'media'
    core_post_2.body = 'ls micro'
    core_post_2.publish_date = dateutil.parser.parse("2022-08-16T12:22:34.059500+00:00")
    core_post_2.is_published = True
    core_post_2.category = core_category_3
    core_post_2.author = core_bloguser_1
    core_post_2.rating = 6
    core_post_2.cover = 'posts/None/Screenshot_from_2022-07-29_20-38-27.png'
    core_post_2 = importer.save_or_locate(core_post_2)

    core_post_3 = Post()
    core_post_3.created_at = dateutil.parser.parse("2022-08-16T12:23:16.032928+00:00")
    core_post_3.updated_at = dateutil.parser.parse("2022-08-18T18:53:26.363374+00:00")
    core_post_3.title = 'tesla'
    core_post_3.body = 'My favorite car brand.'
    core_post_3.publish_date = None
    core_post_3.is_published = False
    core_post_3.category = core_category_2
    core_post_3.author = core_bloguser_1
    core_post_3.rating = 4
    core_post_3.cover = 'posts/3/kan.jpg'
    core_post_3 = importer.save_or_locate(core_post_3)

    core_post_4 = Post()
    core_post_4.created_at = dateutil.parser.parse("2022-08-16T12:25:12.862085+00:00")
    core_post_4.updated_at = dateutil.parser.parse("2022-08-18T18:50:45.607863+00:00")
    core_post_4.title = 'Turtle'
    core_post_4.body = 'There is one important thing about turtles - some kinds of turtles can live for more than a century.'
    core_post_4.publish_date = dateutil.parser.parse("2022-08-18T18:50:45.601313+00:00")
    core_post_4.is_published = True
    core_post_4.category = core_category_5
    core_post_4.author = core_bloguser_1
    core_post_4.rating = 5
    core_post_4.cover = 'posts/4/3_1.png'
    core_post_4 = importer.save_or_locate(core_post_4)

    core_post_5 = Post()
    core_post_5.created_at = dateutil.parser.parse("2022-08-18T14:49:01.147325+00:00")
    core_post_5.updated_at = dateutil.parser.parse("2022-08-20T04:50:04.166150+00:00")
    core_post_5.title = 'post created by dima'
    core_post_5.body = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quis quam porttitor leo lacinia posuere. Phasellus id quam ac urna efficitur sollicitudin. Aliquam ornare in ipsum ut egestas. Aliquam ac erat faucibus, feugiat magna eget, maximus velit. Ut augue tellus, cursus eget congue a, interdum a turpis. Vivamus ornare dui vitae maximus mattis. Duis tempus, nunc non semper ultrices, diam orci suscipit lacus, eu viverra odio nisi ac dui. Ut luctus, lorem vitae interdum tincidunt, sem tortor placerat dui, at consectetur risus mi sit amet nunc.'
    core_post_5.publish_date = dateutil.parser.parse("2022-08-18T14:49:01+00:00")
    core_post_5.is_published = True
    core_post_5.category = core_category_3
    core_post_5.author = core_bloguser_1
    core_post_5.rating = 4
    core_post_5.cover = 'posts/5/dusk.jpg'
    core_post_5 = importer.save_or_locate(core_post_5)

    # Re-processing model: core.models.Comment


    # Re-processing model: core.models.BlogUser


    core_bloguser_2.user_permissions.add(  importer.locate_object(Permission, "id", Permission, "id", 21, {'id': 21, 'name': 'Is user can update post', 'content_type_id': 5, 'codename': 'can_update_post'} )  )


    # Re-processing model: core.models.TagPost

    # Re-processing model: core.models.Post

