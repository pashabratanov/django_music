const user_id = $('#user-id').val();
const room_id = $('#room-id').val();

const ws = new WebSocket(`ws:${window.location.host}/ws/chat/${room_id}/`);


ws.onopen = function() {
    ws.send(JSON.stringify({"type": "connected", "user_id": user_id}));
};


ws.onmessage = function(e) {
    const data = JSON.parse(e.data);

    if (data.type == 'connected') {
        const connected_message = $('<div class="message">' +
            `<b>Bot:</b> <span>${data.message}</span>` +
        '</div>');
        $('#chat-view').append(connected_message);
    } else {
        message_data = JSON.parse(data.message);
        const new_message = $('<div class="message">' +
            `<b>${message_data.username}:</b> <span>${message_data.text}</span>` +
        '</div>');
        $('#chat-view').append(new_message);
    }
};


$('#send-message').click(function() {
    ws.send(
        JSON.stringify({'type': 'message', 'message':  $('#message').val(), 'user_id': user_id})
    );
    $('#message').val('');
});
