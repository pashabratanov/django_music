$('.container button.btn').click(function(event) {
    let data = {
        csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').val()
    };
    let btn = $(this);
    $.post(window.location.href, data, function(data) {
        if (data.is_subscribe) {
            btn.removeClass('btn-primary').addClass('btn-danger');
            btn.text('Unsubscribe');
        } else {
            btn.removeClass('btn-danger').addClass('btn-primary');
            btn.text('Subscribe');
        }
    });
});

