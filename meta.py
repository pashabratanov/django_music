# def some():
#     pass
#
# some()
#
# some.new_var = True
# print(some.new_var)

# class Java:
#     some = 1
#
#     def get_some(self):
#         return self.some
#
#     def set_some(self, value):
#         self.some = value
#
#
# class Python:
#     some = 1

# class A:
#     some = 1
#     __slots__ = ['new']
#
#
# a = A()
# a.new = 2
# a.some = 3
# print(a.new)


# class Some:
#     pass
#
# print(type(Some()))
# print(type(Some))
# print(type(type))


# factory that can produce other classes
# class NoMyMeta(type):
#     def __new__(cls, *args, **kwargs):
#         for attr in list(args[2]):
#             if attr.startswith('my'):
#                 args[2][attr.replace('my_', '')] = args[2][attr]
#                 args[2].pop(attr)
#         args[2]['super_attr'] = 1
#         return super(NoMyMeta, cls).__new__(cls, *args, **kwargs)


# class User(metaclass=NoMyMeta):
#     first_name = 'test'
#     last_name = 'last_test'
#     my_phone = '+38063295634'
#
#     def some(self):
#         print('in some')
#
#
# u = User()
# print(type(User))
# print(u.super_attr)
# print(dir(u))

# User = type('User', (), {'first_name': 'test', 'last_name': 'last_test'})
# user = User()
# print(user.first_name)
# print(type(user))
# print(type(User))

# print(globals())

# import sys
# module = sys.modules[__name__]
# print(dir(module))
# module.User = type('User', (), {'first_name': 'test', 'last_name': 'last_test'})
# u = User()
# print(u.first_name)

# if __name__ == '__main__':
#     class_name = input('Enter class name: ')
#     attributes = {}
#
#     for counter in range(3):
#         attr_name = input(f'enter {counter} attr name: ')
#         attr_val = input(f'enter {counter} attr value: ')
#         attributes[attr_name] = attr_val
#
#     new_class = type(class_name, (), attributes)
#     setattr(module, class_name, new_class)
#
#     new_class_instance = getattr(module, class_name)()
#
#     for attr in attributes.keys():
#         print(f'{attr}: {getattr(new_class_instance, attr)}')
#
#     print(new_class_instance.__class__)

class Zoo:
    animals = ['monkey', 'lion', 'dinosaur']

    def __call__(self, *args, **kwargs):
        print(args, kwargs)

    def __bool__(self):
        return bool(self.animals)

    def __len__(self):
        return len(self.animals)

    def __add__(self, other):
        if isinstance(other, Zoo):
            self.animals += other.animals
            other.animals = []
        elif isinstance(other, str):
            self.animals.append(other)
        return self

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


#
# zoo = Zoo()
# zoo1 = Zoo()
#
# zoo += zoo1
# zoo + 'elephant'
# print(zoo.animals)
# print(zoo1.animals)


# def pp(a: int, b: int):
#     """
#
#     :param a: int
#     :param b: int
#     :return: float
#     """
#     return a / b
#
# print(help(pp))

# global_variable = 100
#
# def bar():
#     print(global_variable)
#     global_variable = 200
#
# bar()

# if __name__ == '__main__':
#     print('mmm')

def list_updater(val, lst=[]):
    lst.append(val)
    return lst

res = list_updater(1)
print(res)
res = list_updater(3)
print(res)