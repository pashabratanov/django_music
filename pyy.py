global_variable = 100


def foo():
    global global_variable
    print('inside function', global_variable)
    global_variable += 1

foo()
print(global_variable)
foo()
print(global_variable)
print(dir())